[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/chiefeh/ansible-laptop-config)

# Ansible Laptop Config

Setup of my environment, [using this article as a reference](https://www.redhat.com/sysadmin/ansible-configure-vim)

Clone the repo to your computer and cd to the directory
```
git clone https://gitlab.com/chiefeh/ansible-laptop-config.git
cd ansible-laptop-config
```
run the playbook
```
ansible-playbook vim-config.yml
```
open vim to see the nice VI IMproved display

![vim screenshot](vim.png)
